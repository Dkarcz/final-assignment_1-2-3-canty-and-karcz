#ifndef STORAGE_MGR_H
#define STORAGE_MGR_H

#include "dberror.h"
#include "stdlib.h"
#include "string.h"
/************************************************************
 *                    handle data structures                *
 ************************************************************/
typedef struct SM_FileHandle {
  char *fileName;
  int totalNumPages;
  int curPagePos;
  void *mgmtInfo;
} SM_FileHandle;

typedef char* SM_PageHandle;

/************************************************************
 *                    interface                             *
 ************************************************************/
/* manipulating page files */
extern void initStorageManager (void);
extern RC createPageFile (char *fileName);
extern RC openPageFile (char *fileName, SM_FileHandle *fHandle);
extern RC closePageFile (SM_FileHandle *fHandle);
extern RC destroyPageFile (char *fileName);

/* reading blocks from disc */
extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage);
extern int getBlockPos (SM_FileHandle *fHandle);
extern RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);

/* writing blocks to a page file */
extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC appendEmptyBlock (SM_FileHandle *fHandle);
extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle);

#endif
RC findFileHandle(SM_FileHandle *fHandle);
/*Returns 0(RC_OK) if everything is okay 
Returns 4(RC_FILE_NOT_FOUND) if we couldn't find the SM_FileHandle*/
RC findFileName(char *fileName, SM_FileHandle *fHandle);
/*Similar to findFileHandle
Finds the file by name and points fHandle to the corresponding SM_FileHandle*/

SM_FileHandle **listOfFileHandles;
int totalNumFiles;


RC findFileHandle(SM_FileHandle *fHandle){
	/*Can be improved*/
	int i;
	for(i = 0; i < totalNumFiles; i++){
		if(listOfFileHandles[i]==fHandle)
			break;
		else
			continue;
	}
	if(i == totalNumFiles)
		return RC_FILE_NOT_FOUND;
	return RC_OK;
}
RC findFileName(char *fileName, SM_FileHandle *fHandle){
	/*Can be improved*/
	int i;
	for(i = 0; i < totalNumFiles; i++){
		if(strcmp(listOfFileHandles[i]->fileName, fileName))
			continue;
		/*How to account for multiple files with same name?*/
		else{
			fHandle = listOfFileHandles[i];
			break;
		}
	}
	if(i == totalNumFiles)
		return RC_FILE_NOT_FOUND;
	return RC_OK;
}

void initStorageManager(){
	/*Initializes our array of File Handles*/
	listOfFileHandles = (malloc(sizeof(SM_FileHandle)));
	totalNumFiles = 0;
}
RC createPageFile(char *fileName){
	listOfFileHandles[totalNumFiles]->fileName = fileName;
	listOfFileHandles[totalNumFiles]->totalNumPages = 1;
	listOfFileHandles[totalNumFiles]->mgmtInfo = malloc(sizeof(SM_PageHandle));
	/* Assumes that we will always have memory for more pages */
	totalNumFiles++;
	listOfFileHandles[totalNumFiles] = malloc(sizeof(SM_FileHandle));
	return RC_OK;
}
RC openPageFile(char *fileName, SM_FileHandle *fHandle){
	int result;
	result = findFileHandle(fHandle);
	/*Finds SM_FileHandle, if not found create a file*/
	if(result == RC_FILE_NOT_FOUND)
		result = createPageFile(fileName);
	fHandle->curPagePos = 0;
	return result;
}
RC closePageFile(SM_FileHandle *fHandle){
	int result;
	result = findFileHandle(fHandle);
	if(result == RC_FILE_NOT_FOUND)
		return RC_FILE_NOT_FOUND;
	fHandle->curPagePos = -1;
	return RC_OK;
}
RC destroyPageFile(char *fileName){
	int result;
	SM_FileHandle *fHandle;
	result = findFileName(fileName, fHandle);
	if(result == RC_FILE_NOT_FOUND)
		return RC_FILE_NOT_FOUND;
	/* Can be improved */
	fHandle = (SM_FileHandle *)NULL;
	return RC_OK;
}

RC readBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
	int result;
	SM_PageHandle *listOfPages = (SM_PageHandle *)fHandle->mgmtInfo;
	result = findFileHandle(fHandle);
	if(result == RC_FILE_NOT_FOUND)
		return RC_FILE_NOT_FOUND;
	if(pageNum >= fHandle->totalNumPages || pageNum < 0)
		return RC_READ_NON_EXISTING_PAGE;
	memcpy(&memPage,listOfPages[pageNum], sizeof(SM_PageHandle));
	fHandle->curPagePos = pageNum;
	
	return RC_OK;
}
int getBlockPos(SM_FileHandle *fHandle){
	return fHandle->curPagePos;
}
RC readFirstBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int result;
	result = findFileHandle(fHandle);
	
	if(result == RC_FILE_NOT_FOUND)
		return RC_FILE_NOT_FOUND;
	
	memcpy(&memPage,fHandle->mgmtInfo, sizeof(SM_PageHandle));
	fHandle->curPagePos = 0;
	
	return RC_OK;
}
RC readCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int result;
	int curPagePos = fHandle->curPagePos;
        SM_PageHandle *listOfPages = (SM_PageHandle *)fHandle->mgmtInfo;

	result = findFileHandle(fHandle);
	
	if(result == RC_FILE_NOT_FOUND)
		return RC_FILE_NOT_FOUND;
	if(curPagePos >= fHandle->totalNumPages || curPagePos < 0)
		return RC_READ_NON_EXISTING_PAGE;
	
	memcpy(&memPage,listOfPages[curPagePos], sizeof(SM_PageHandle));
	
	return RC_OK;
}
RC readNextBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int result;
	result = findFileHandle(fHandle);
	if(result == RC_FILE_NOT_FOUND)
		return RC_FILE_NOT_FOUND;

	int curPagePos = fHandle->curPagePos;
        SM_PageHandle *listOfPages = (SM_PageHandle *)fHandle->mgmtInfo;
	curPagePos++;
	
	if(curPagePos >= fHandle->totalNumPages || curPagePos < 0)
		return RC_READ_NON_EXISTING_PAGE;
	
	memcpy(&memPage,listOfPages[curPagePos], sizeof(SM_PageHandle));
	fHandle->curPagePos = curPagePos;
	
	return RC_OK;
}
RC readLastBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int result;	
	result = findFileHandle(fHandle);
	
	if(result == RC_FILE_NOT_FOUND)
		return RC_FILE_NOT_FOUND;
	
	int lastPagePos = fHandle->totalNumPages-1;
	SM_PageHandle *listOfPages = (SM_PageHandle *)fHandle->mgmtInfo;

	memcpy(&memPage,listOfPages[lastPagePos], sizeof(SM_PageHandle));
	
	return RC_OK;
}
