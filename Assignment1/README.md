Team members:

1)	Jelani Canty
2)	Daniel Karcz

All code is commented. 

Present below is just the general defined outline structure.  

Assignment 1: Storage Manager

Storage Manager: Implement a storage manager that allows read/writing of blocks to/from a file on disk

1) dberror.c, 2) dberror.h, 3) storage_mgr.c, 4) storage_mgr.h, 5) test_assign1_1.c, 11) 6) test_helper.h


Organization of File functions

File Related Methods
createPageFile: Create a new page file “filename”. The initial file size is one page of 4096.  The function used to create the file is fopen (w+ mode).  If RC_FILE_NOT_FOUND is returned, the if the file could not be generated. 

openPageFile: Opens an existing page file. Returns RC_FILE_NOT_FOUND if the file does not exist

closePageFile, destroyPageFile: Close an open page file or destroy (delete) a page file.

Read and Write Methods: Two types of read and write methods have been implemented: methods with absolute addressing(readBlock) and methods that address relative to the current page of a file (readNextBlock).

readBlock: This method reads the pageNum block from a file and stores its content in the memory pointed to by the memPage page handle. If the file has less than pageNum pages, the method is returned to RC_READ_NON_EXISTING_PAGE.

getBlockPos: Return the current page position in a file.  Returns fhandle -> curPagePos

readFirstBlock, readLastBlock: Read the first respective last page in a file

readPreviousBlock, readCurrentBlock, readNextBlock: Reads the current, previous, or next page relative to the curPagePos of the file. The curPagePos is moved to the page that was read.

writeBlock, writeCurrentBlock: Writes a page to disk.

appendEmptyBlock: Increase the number of pages in the file by one. The new last page is filled with zero bytes.

ensureCapacity: If the file has less than numberOfPages pages, then the file is increased.  
 
